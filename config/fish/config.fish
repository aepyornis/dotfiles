set fish_greeting

# umask 077

if test -d "$HOME/.asdf/shims"
    fish_add_path --prepend --move "$HOME/.asdf/shims"
end

if test -d ~/.local/share/pnpm
    set -gx PNPM_HOME ~/.local/share/pnpm
end

if set -q INSIDE_EMACS; and set -q EMACS_VTERM_PATH
    source $EMACS_VTERM_PATH/etc/emacs-vterm.fish
end

set -gx EDITOR emacsclient -nw -a mg

if test -f ~/Sync/config/env.fish
    source ~/Sync/config/env.fish
end

if status is-interactive
    abbr --add e "emacsclient -t"
    abbr --add be "bundle exec"
    abbr --add ber "bundle exec rails"
    abbr --add bet "bundle exec rails test"
    abbr --add d "docker"
    abbr --add dc "docker compose"
    abbr --add j "jump"
    abbr --add g "git"
    abbr --add p "pnpm"
    abbr --add s "systemctl"
    abbr --add ss "sudo systemctl"
    abbr --add su "systemctl --user"
end

if status is-login
    echo "welcome"
    if test (tty) = "/dev/tty1"
        if test -f ~/.config/sway/enable
             set -x XDG_CURRENT_DESKTOP sway
             # exec sway -d > ~/.cache/sway.log 2>&1
            exec sway
        end
    end
end
