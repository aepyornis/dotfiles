function jump
    set cachefile ~/.cache/.jump

    if test ! -f $cachefile; or test (math "$(date +%s)-3600") -gt (stat -c %Y $cachefile)
        if test -f $cachefile
            rm $cachefile
        end
        fd --max-depth 1 . $HOME >> $cachefile
        fd -H -I --type d --max-depth 4 --glob ".git" ~/code | ruby -n -e 'puts $_[0..-7]' >> $cachefile
        if test -f ~/Sync
            fd --exclude encrypted --type d --max-depth 3 . ~/Sync >> $cachefile
        end
    end

    cd (cat $cachefile | fzf)
    clear
end
