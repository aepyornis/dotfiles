function go-bin-link -a exe
    if test -x ~/go/bin/$exe
        ln -f -v -s ../../go/bin/$exe ~/.local/bin/$exe
    else
        echo "missing ~/go/bin/$exe" 1>&2
        return 1
    end
end
