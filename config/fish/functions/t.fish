function __tmux_always_join -a name
    if not tmux has -t $name 2> /dev/null
        set repos (fd -H -I --type d --max-depth 4 --glob ".git" ~/code | string split '\n')

        for repo in $repos
            set p (path resolve "$repo../")
            if test (path basename $p) = $name
                cd $p
                break
            end
        end
        tmux new -s $name -d
    end

    if set -q TMUX
        tmux switch-client -t $name
    else
        tmux attach-session -d -t $name
    end
end

function t
    if test -z $argv[1]
	__tmux_always_join main
    else if test $argv[1] = 'ls'
	tmux ls | awk '{ print $1, $2, $3 }'
    else if test $argv[1] = 'rm' || test $argv[1] = 'remove'
	tmux kill-session -t $argv[2]
    else if test $argv[1] = 'd' || test $argv[1] = 'detach'
	if set -q TMUX
            tmux detach
	end
    else if test $argv[1] = 'a'
	__tmux_always_join $argv[2]
    else
	__tmux_always_join $argv[1]
    end
end
