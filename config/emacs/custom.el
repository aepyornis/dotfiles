(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Man-notify-method 'thrifty)
 '(custom-safe-themes
   '("1d6b446390c172036395b3b87b75321cc6af7723c7545b28379b46cc1ae0af9e"
     "8d146df8bd640320d5ca94d2913392bc6f763d5bc2bb47bed8e14975017eea91"
     "84b3c4fa1bbccd01a173839b7eebc226105fafd6b108f8400995eb79c67c9adf"
     "702d0136433ca65a7aaf7cc8366bd75e983fe02f6e572233230a528f25516f7e"
     "704f75d46620d87bb246e2ec1abb129437764b0e84ac0fff6b968311cc046918"
     default))
 '(desktop-load-locked-desktop nil)
 '(desktop-save 'ask-if-new t)
 '(devdocs-window-select t)
 '(markdown-fontify-code-blocks-natively t)
 '(notmuch-show-all-tags-list t)
 '(package-selected-packages
   '(ace-window apheleia async auto-compile bind-key bufler
		caddyfile-mode cape clojure-mode consult consult-eglot
		consult-flycheck corfu corfu-terminal crux d2-mode
		dash deadgrep devdocs diminish dired-sidebar
		edit-indirect editorconfig ef-themes eglot
		eglot-booster eldoc erc exec-path-from-shell eyebrowse
		f faceup fish-completion fish-mode flycheck
		flycheck-eglot flymake-proselint go go-mode haml-mode
		ht htmlize idlwave indent-bars inf-ruby jinx jsonrpc
		langtool lua-mode magit-gerrit marginalia modus-themes
		nginx-mode no-littering notmuch nov olivetti orderless
		org orgit-forge pdf-tools php-mode project puni python
		rust-mode s soap-client spacious-padding systemd tramp
		ts use-package use-package-ensure-system-package
		verilog-mode vertico vterm web-mode which-key
		window-tool-bar xref yaml-mode yard-mode
		yasnippet-snippets))
 '(package-vc-selected-packages
   '((eglot-booster :vc-backend Git :url
		    "https://github.com/jdtsmith/eglot-booster")
     (indent-bars :vc-backend Git :url
		  "https://github.com/jdtsmith/indent-bars")))
 '(safe-local-variable-values
   '((js-indent-level . 2) (js-indent-level . 4)
     (devdocs-current-docs "rails~7.1" "ruby~3.3")
     (magit-todos-exclude-globs "makem.sh" "Makefile")
     (org-export-initial-scope . buffer) (whitespace-line-column . 80)
     (flymake-mode . -1) (checkdoc-minor-mode . t) (flymake-mode)))
 '(vue-html-extra-indent 4))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mmm-default-submode-face ((t nil))))
