;; ziggy's emacs configuration  -*- lexical-binding: t -*-

;; early set variables
(setq load-prefer-newer t)
(setq read-process-output-max (* 1024 1024)) ; 1mb /proc/sys/fs/pipe-max-size
(setq native-comp-async-jobs-number 4)
(setq native-comp-async-report-warnings-errors 'silent)
(setq eyebrowse-keymap-prefix (kbd "M-i"))

;; setup packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(setopt package-native-compile t)
(setopt package-install-upgrade-built-in t)
(package-initialize)

;; enable auto compile
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)

;; no-littering tells packages to save their data in ~/.emacs.d/var and ~/.emacs.d/etc
(require 'no-littering)
(no-littering-theme-backups)

;; use path defined in fish
(require 'exec-path-from-shell)
(setq shell-file-name (executable-find "fish"))
(setq explicit-shell-file-name shell-file-name)
(add-to-list 'exec-path-from-shell-variables "SSH_AUTH_SOCK")
(exec-path-from-shell-initialize)

;; load some utilities
(require 'dash)
(require 's)
(require 'f)
(require 'async)
(require 'crux)
(require 'cape)
(require 'corfu)
(require 'eldoc)
(require 'jinx)
(require 'project)
(require 'magit)

;; load custom.el
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file) (load custom-file))

;; Global modes
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))     ; hide tool bar
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1)) ; hide scroll bar
(menu-bar-mode -1)              ; no menu bar
(tooltip-mode -1)               ; no tooltips
(blink-cursor-mode -1)          ; no blinking cursor
(display-time-mode -1)          ; hide time
(delete-selection-mode t)       ; delete the selection with a keypress
(savehist-mode t)               ; saving of minibuffer history
(recentf-mode t)                ; remember opened files
(save-place-mode t)             ; remember place in file
(vertico-mode t)                ; VERTical Interactive COmpletion
(global-corfu-mode t)           ; tab complete
(corfu-popupinfo-mode t)        ; show docs while browsing candidates
(marginalia-mode t)             ; add descriptions for M-x
(which-key-mode t)              ; keybinding hints
(show-paren-mode t)             ; highlight parentheses pairs
(electric-pair-mode t)          ; automatic parens pairing
(electric-indent-mode t)        ; on-the-fly reindentation of text lines
(puni-global-mode t)            ; parentheses universalistic
(global-auto-revert-mode t)     ; revert buffers automatically when underlying files are changed externally
(auto-save-visited-mode t)      ; Auto-save files every X seconds
;;(yas-global-mode t)             ; yasnippet
(spacious-padding-mode t)       ; more padding
(winner-mode t)                 ; remember windows
(global-flycheck-mode +1)       ; flycheck
;; (eyebrowse-mode t)              ; workspaces
;; (global-display-line-numbers-mode t) ; show line numbers
(tab-bar-mode t)                ; enable tab bar
;; (global-tab-line-mode t)      : tab line on top showing windows displaying the current buffer
;; (global-hl-line-mode t)         ; highlight current line

;; Preferences
(setopt epa-file-encrypt-to '("E5E23F4441A6CCB6A443060382987B3FCDCCDDB7"))       ; encrypt files to my key
(setopt auth-sources (list (expand-file-name "~/Sync/config/authinfo.gpg")))     ; store encrypted credentials
(setopt personal-elisp-file (expand-file-name "~/Sync/config/personal.el"))      ; personal.el is run at startup
(setopt org-directory "~/Sync/org")
(setopt org-notes-publishing-directory "/var/www/html/org")
(setopt my-scratch-buffer-menu-options '("ruby" "js" "sh" "fish" "python" "sql" "text" "org" "emacs-lisp"  "html" "haml" "c" "go")) ; C-c s scratch menu
(setopt browse-url-browser-function 'browse-url-firefox) ;; 'browse-url-emacs 'browse-url-chromium
(setopt large-file-warning-threshold 100000000) ; 100mb
(setopt inhibit-startup-screen t)       ; disable startup screen
(setopt initial-major-mode 'text-mode)  ; text-mode = initial scratch buffer mode
(setopt initial-scratch-message "")     ; hide scratch message
(setopt ring-bell-function 'ignore)     ; disable bell ring
(setopt visible-cursor nil)             ; fixes blinking cursor issue in terminal
(setopt create-lockfiles nil)           ; don't use lockfiles
(setopt auto-save-default nil)          ; turn off auto-saves
(setopt auto-save-visited-interval 60)  ; for auto-save-visited default is 5
(setopt kill-ring-max 1000)             ; default is 120
(setopt kill-do-not-save-duplicates t)  ; no duplicates in killring
(setopt save-interprogram-paste-before-kill t) ; save clipboard to kill-ring
(setopt history-length 1000)            ; higher default history
(setopt history-delete-duplicates t)    ; no duplicates in history
(setopt recentf-max-saved-items 500)    ; recentf list
(setopt select-enable-clipboard t)      ; enable clipboard selection
(setopt vc-follow-symlinks t)           ; follow symlinks
(setopt confirm-kill-processes nil)     ; don't ask to terminate emacs's processes
(setopt confirm-kill-emacs 'y-or-n-p)   ; but do warn before quitting
(setopt use-short-answers t)            ; use y/n instead of yes/no
(setopt tab-always-indent 'complete)    ; indent then complete
(setopt project-vc-extra-root-markers '(".project.el")) ; use a blank .project.el file if there is no .git
(setopt dired-auto-revert-buffer t)                     ; dired revert on re-visiting
(setopt dired-ls-F-marks-symlinks t)                    ; dired show symlinks
(setopt dired-kill-when-opening-new-dired-buffer t)     ; kill the current buffer when selecting a new directory
(setopt dired-recursive-copies 'always)                 ; copy recursively
(setopt tab-bar-new-tab-choice "*scratch*")  ; show scratch on new tabs
(setopt switch-to-buffer-obey-display-actions t) ; If non-nil, `switch-to-buffer' runs `pop-to-buffer-same-window' instead
(setopt xref-show-xrefs-function #'consult-xref) ; use consult for xref
(setopt xref-show-definitions-function #'consult-xref)
(setopt xref-auto-jump-to-first-xref t) ; "t = jumps to the first result. `show' = show the first result's location. `move' = move point to the first result
(setopt doc-view-mupdf-use-svg (image-type-available-p 'svg))
(setopt find-function-C-source-directory "~/.local/src/emacs/src")  ;; git clone --depth 1 -b emacs-29 https://github.com/emacs-mirror/emacs.git ~/.local/src/emacs
(setopt global-auto-revert-non-file-buffers t) ; auto update dired and such
(setopt auto-revert-interval 15)              ; default is 5
(setopt desktop-auto-save-timeout 120)        ; every 2 minutes
(setopt desktop-restore-forces-onscreen nil)  ; don't force frames onscreen when restoring
(setopt desktop-save t)                       ; always  save
(setopt vterm-max-scrollback 5000)            ; vterm scrollback
(setopt tramp-use-connection-share nil)       ; instead use configs in ~/.ssh/config
(setopt tramp-verbose 4)                      ; tramp debug
(setopt auth-source-debug nil)                ; set to t to debug auth-sources
(setopt dictionary-server "localhost")        ; apt install dictd dict-wn dict-gcide dict-vera dict-foldoc dict-jargon
(setopt Info-hide-note-references nil)        ; show info notes and references
(setopt aw-scope 'frame)                      ; M-o is per frame
(setopt flycheck-idle-change-delay 1.5)       ; wait before rechecking
(setopt browse-url-firefox-program "flatpak"  ; use flatpak to run firefox
        browse-url-firefox-arguments '("run" "io.gitlab.librewolf-community"))
(setopt treesit-font-lock-level 4)            ; more colors

;; make M-x work as expected on macs
(when (eq system-type 'darwin)
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier nil))

;; Theme
;; Default Font
;;(set-face-attribute 'default nil :family "Hack" :height 140)
(set-face-attribute 'default nil :family "Hack" :height 161)
;; Monospaced typeface
;;(set-face-attribute 'fixed-pitch nil :family "Hack")
;; Proportionately spaced typeface
(set-face-attribute 'variable-pitch nil :family "DejaVu Serif" :height 1.0)

(with-eval-after-load "flymake"
  (set-face-attribute 'flymake-note nil :underline 'unspecified)  ; hide flymake underlines
  (setopt flymake-mode-line-lighter "🪰")
  (setopt flymake-no-changes-timeout 4.0)
  )

;; set frame title see mode-line-format
;;(setq-default frame-title-format '("%b"))

;; Modeline
(setopt mode-line-format
	'("%e" mode-line-front-space
	  (:propertize ("" mode-line-modified mode-line-remote) display (min-width (3.0)))
	  " " mode-line-buffer-identification
	  " %l:%p "
	  (vc-mode vc-mode)
	  "  " mode-line-modes mode-line-misc-info mode-line-end-spaces
	))

;; rename minor modes
(diminish 'jinx-mode " j")
(diminish 'yas-minor-mode " y")
(diminish 'eldoc-mode " eldoc")
(diminish 'which-key-mode)

;; enable functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; hide warning popups
(add-to-list 'display-buffer-alist
             '("\\`\\*\\(Warnings\\|Compile-Log\\)\\*\\'"
               (display-buffer-no-window)
               (allow-no-window . t)))

;; save more history
(add-to-list 'savehist-additional-variables 'corfu-history)
(add-to-list 'savehist-additional-variables 'command-history)
(add-to-list 'savehist-additional-variables 'regexp-search-ring)

;; 2 spaces please
;;(setq-default indent-tabs-mode nil)
(setopt css-indent-offset 2)
(setopt js-indent-level 2)
(setopt nginx-indent-level 2)
(setopt typescript-indent-level 2)
(setopt web-mode-attr-indent-offset 2)
(setopt web-mode-auto-closeq-style 2)
(setopt web-mode-code-indent-offset 2)
(setopt web-mode-css-indent-offset 2)
(setopt web-mode-markup-indent-offset 2)
;; Langs
(setopt web-mode-enable-auto-closing t)
(setopt web-mode-enable-auto-pairing t)
(setopt web-mode-enable-css-colorization t)
(setopt web-mode-enable-current-element-highlight t)
(setopt ruby-insert-encoding-magic-comment nil)
(setopt inf-ruby-console-environment "development")
;; text
(setopt langtool-language-tool-jar "~/.local/opt/LanguageTool-6.5/languagetool-commandline.jar")

;; Completion
;; corfu https://github.com/minad/corfu/wiki
;; cape https://github.com/minad/cape
;; orderless https://github.com/oantolin/orderless
;; eglot (builtin)
(setopt tab-always-indent 'complete) ;; indent then complete
(setopt completion-cycle-threshold 3)
(setopt completion-styles '(orderless partial-completion basic))
(setopt completion-category-defaults nil)
(setopt completion-category-overrides '(
				      (file (styles basic partial-completion))
				      (eglot (styles orderless))
                                      (eglot-capf (styles orderless))
				      ))
(setopt corfu-cycle t)
(setopt corfu-quit-no-match 'separator)
(setopt corfu-auto 1) ;; enable or disable auto completion
(setopt corfu-auto-delay 0.05)
(setopt corfu-auto-prefix 3)
(setopt corfu-popupinfo-delay '(1.0 . 0.25))

(define-key corfu-map (kbd "M-SPC") 'corfu-insert-separator)

(add-hook 'completion-at-point-functions #'cape-file)
;;(add-hook 'completion-at-point-functions #'cape-dabbrev)

(with-eval-after-load "eglot"
  ;; (when (package-installed-p 'eglot-ltex-plus)
  ;;     (require 'eglot-ltex-plus))
  ;; (when (package-installed-p 'eglot-booster)
  ;;   (eglot-booster-mode))

  (setq eglot-stay-out-of '(flymake))

  (fset #'jsonrpc--log-event #'ignore)
  ;; always use bundle for ruby's solargraph
  (add-to-list 'eglot-server-programs
	       `((ruby-mode ruby-ts-mode) . ("bundle" "exec" "solargraph" "stdio" :initializationOptions (:diagnostics t)))
	       )

  (define-key eglot-mode-map (kbd "M-.") 'xref-find-definitions)
  (define-key eglot-mode-map (kbd "C-c o") 'eglot-code-action-organize-imports)
  (define-key eglot-mode-map (kbd "C-c F") 'eglot-format-buffer)

  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster) ; solves eglot caching issue
  )

(with-eval-after-load "org"
  (require 'htmlize)
  (require 'ox-publish)
  ;; (setq org-archive-save-context-info '())
  (setopt org-html-doctype "html5")
  (setopt org-html-html5-fancy t)
  (setopt org-html-htmlize-output-type 'inline-css)
  (setopt org-html-validation-link nil)
  (setopt org-src-fontify-natively t)
  (add-to-list 'display-buffer-alist '("\\*Org Src" (display-buffer-same-window)))
  (add-to-list 'display-buffer-alist '("\\*Org Select\\*" (display-buffer-at-bottom)))
  )

;; email
(setopt message-send-mail-function 'message-send-mail-with-sendmail)
(setopt sendmail-program "/usr/bin/msmtp")
(setopt mail-specify-envelope-from t)
(setopt mail-envelope-from 'header)
(setopt message-sendmail-envelope-from 'header)
(setopt notmuch-fcc-dirs "sent +sent -unread -inbox")

;; magit
(setopt magit-repository-directories '(("~/code" . 4)))
(setopt magit-list-refs-sortby "-creatordate")
(with-eval-after-load "magit"
  (require 'forge)
  ;; also required: git config --global gitlab.0xacab.org/api/v4.user <USERNAME>
  (add-to-list 'forge-alist '("0xacab.org" "0xacab.org/api/v4" "0xacab.org" forge-gitlab-repository))
  )

(with-eval-after-load "calendar"
  (setq diary-date-forms diary-iso-date-forms)
  )

(with-eval-after-load "vterm"
  (add-to-list 'vterm-tramp-shells '("doas" "/usr/bin/fish"))
  (define-key vterm-mode-map (kbd "M-i") nil)
  ;; https://www.masteringemacs.org/article/demystifying-emacs-window-manager
  (add-to-list 'display-buffer-alist
	       '("\\*vterm\\*" display-buffer-reuse-mode-window
		 ;; change to `t' to not reuse same window
		 (inhibit-same-window . nil)
		 (mode vterm-mode vterm-copy-mode)))

  (add-hook 'vterm-mode-hook  'with-editor-export-editor)
  )

;; eyebrowse
(with-eval-after-load "eyebrowse"
  (setopt eyebrowse-mode-line-style 'always)
  (setopt eyebrowse-mode-line-separator "|")
  (setopt eyebrowse-new-workspace t)
  (define-key eyebrowse-mode-prefix-map "l" 'eyebrowse-last-window-config)
  )

;; bufler
(with-eval-after-load "bufler"
  (setf bufler-groups
      (append (bufler-defgroups
                (group
                 (mode-match "*notmuch*" (rx bos "notmuch")))
		)
	      bufler-groups))
  )

;; tree-sitter sources
(setq treesit-language-source-alist
      '(
	(bash "https://github.com/tree-sitter/tree-sitter-bash")
	(css "https://github.com/tree-sitter/tree-sitter-css")
	(elisp "https://github.com/Wilfred/tree-sitter-elisp")
	(go "https://github.com/tree-sitter/tree-sitter-go")
        (gomod "https://github.com/camdencheek/tree-sitter-go-mod")
        (html "https://github.com/tree-sitter/tree-sitter-html")
	(javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
	(json "https://github.com/tree-sitter/tree-sitter-json")
	(python "https://github.com/tree-sitter/tree-sitter-python")
	(ruby "https://github.com/tree-sitter/tree-sitter-ruby")
	(rust "https://github.com/tree-sitter/tree-sitter-rust")
	(tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
	(typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
	(yaml "https://github.com/ikatyang/tree-sitter-yaml")
	(dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile")
     ))

;; use tree-sitter versions
(dolist (mapping '(
		   (python-mode . python-ts-mode)
                   (css-mode . css-ts-mode)
                   (typescript-mode . typescript-ts-mode)
                   (json-mode . json-ts-mode)
                   (js-mode . js-ts-mode)
                   (css-mode . css-ts-mode)
		   (ruby-mode . ruby-ts-mode)
                   (yaml-mode . yaml-ts-mode)
                   (go-mode . go-ts-mode)
		   ))
  (add-to-list 'major-mode-remap-alist mapping)
  )

;; AutoModeAlist
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mjs\\'" . js-ts-mode))
(add-to-list 'auto-mode-alist '("\\.cjs\\'" . js-ts-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . tsx-ts-mode))
(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode))
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
(add-to-list 'auto-mode-alist '("\\.\\(service\\|timer\\|mount\\|network\\)\\'" . systemd-mode))
(add-to-list 'auto-mode-alist '("\\(Dockerfile\\|Containerfile\\)$" . dockerfile-ts-mode))
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;; Hooks
(add-hook 'before-save-hook #'delete-trailing-whitespace)  ; delete trailing whitespace from all files

(defun my-eww-after-render-hook ()
  "Rename eww buffer after loading."
  (let* ((title  (plist-get eww-data :title))
         (url    (plist-get eww-data :url))
         (result (concat "*eww-" (or title
                              (if (string-match "://" url)
                                  (substring url (match-beginning 0))
                                url)) "*")))
    (rename-buffer result t)))

(add-hook 'eww-after-render-hook #'my-eww-after-render-hook)

;; Shell hooks
(defun my-shell-mode-hook ()
  (setq-local corfu-auto nil)
  (setq-local global-hl-line-mode nil)
  (puni-disable-puni-mode)
  )

(add-hook 'eshell-mode-hook #'my-shell-mode-hook)
(add-hook 'vterm-mode-hook #'my-shell-mode-hook)

;; Mode Hooks
(defun my-programing-mode-hook ()
  (toggle-truncate-lines 1)
  (hs-minor-mode)
  )

(defun my-text-mode-hook ()
  (jinx-mode t)
  (toggle-truncate-lines -1)
  (toggle-word-wrap 1)
  (setq-local corfu-auto -1)
  (puni-disable-puni-mode)
  )

(add-hook 'prog-mode-hook #'my-programing-mode-hook)
(add-hook 'text-mode-hook #'my-text-mode-hook)
(add-hook 'conf-mode-hook #'puni-disable-puni-mode)
(add-hook 'yaml-mode-hook (lambda () (jinx-mode -1)))

(add-hook 'python-base-mode-hook #'indent-bars-mode)
(add-hook 'ruby-base-mode-hook #'indent-bars-mode)

;;(add-hook 'go-ts-mode-hook #'apheleia-mode)
;;(add-hook 'ruby-base-mode-hook #'yard-mode)

;; Global Key Map
(global-unset-key (kbd "C-z"))  ; unset suspend-frame
(global-unset-key (kbd "M-u"))  ; unset upcase-word
;; F keys
(keymap-global-set "M-S-<f8>" 'my-snippet-set)
(keymap-global-set "M-S-<f9>" 'my-snippet-insert)
;; M-
(keymap-global-set "M-y" 'consult-yank-pop) ;yank-pop
(keymap-global-set "M-l" 'jinx-correct) ; normally bound to 'downcase-word
(keymap-global-set "M-o" 'ace-window)
(keymap-global-set "M-i" 'consult-imenu)
;; scroll up or down
(keymap-global-set "M-<up>" 'scroll-other-window-down)
(keymap-global-set "M-<down>" 'scroll-other-window)
;; M-p Cape
(define-prefix-command 'cape-keymap)
(keymap-global-set "M-p" 'cape-keymap)
(define-key cape-keymap "p" 'completion-at-point)
(define-key cape-keymap "d" 'cape-dabbrev)
(define-key cape-keymap "f" 'cape-file)
(define-key cape-keymap "w" 'cape-dict)
(define-key cape-keymap "h" 'cape-history)
;; M-g bindings (goto-map)
(keymap-global-set "M-g e" 'consult-compile-error)
(keymap-global-set "M-g b" 'consult-bookmark)
(keymap-global-set "M-g f" 'consult-flycheck)
(keymap-global-set "M-g g" 'consult-goto-line)
(keymap-global-set "M-g i" 'consult-imenu)
(keymap-global-set "M-g I" 'consult-imenu-multi)
;; M-s bindings (search-map)
(keymap-global-set "M-s d" 'consult-find)
(keymap-global-set "M-s r" 'consult-ripgrep)
(keymap-global-set "M-s l" 'consult-line)
(keymap-global-set "M-s L" 'consult-line-multi)
;; C-
(keymap-global-set "C-+" 'text-scale-increase)
(keymap-global-set "C--" 'text-scale-decrease)
(keymap-global-set "C-a" 'crux-move-beginning-of-line) ; rebind
;;(keymap-global-set "C-k" 'crux-smart-kill-line) ; rebind
(keymap-global-set "C-<tab>" 'hs-cycle)
(keymap-global-set "C-<iso-lefttab>" 'hs-global-cycle)
;; C-x
(keymap-global-set "C-x g" 'magit-status)
(keymap-global-set "C-x b" 'consult-buffer)
(keymap-global-set "C-x C-b" 'bufler)
(keymap-global-set "C-x C-n" 'dired-sidebar-toggle-sidebar)
;; C-c
(keymap-global-set "C-c r" 'crux-recentf-find-file)
(keymap-global-set "C-c R" 'crux-rename-buffer-and-file)
(keymap-global-set "C-c n" 'crux-cleanup-buffer-or-region)
(keymap-global-set "C-c s" 'scratch-buffer-menu)
(keymap-global-set "C-c i" 'ielm)
(keymap-global-set "C-c h" 'eldoc)
(keymap-global-set "C-c d" 'deadgrep)
(keymap-global-set "C-c l" 'langtool-check)
(keymap-global-set "C-c f" 'flycheck-next-error)
(keymap-global-set "C-c v" 'project-vterm-or-vterm)
;; C-c m change mode shortcuts
(define-prefix-command 'changemode-map)
(keymap-global-set "C-c m"  'changemode-map)
(define-key changemode-map "a" 'auto-fill-mode)
(define-key changemode-map "t" 'toggle-truncate-lines)
(define-key changemode-map "T" 'tab-bar-mode)
(define-key changemode-map "L" 'tab-line-mode)
(define-key changemode-map "w" 'toggle-word-wrap)
(define-key changemode-map "f" 'flycheck-mode)
;;(define-key changemode-map "a" 'aggressive-indent-mode)
(define-key changemode-map "e" 'eldoc-mode)
(define-key changemode-map "h" 'global-hl-line-mode)
(define-key changemode-map "j" 'jinx-mode)
;; C-z map
(define-prefix-command 'control-z-map)
(keymap-global-set "C-z" 'control-z-map)
(define-key control-z-map "n" 'notmuch)
(define-key control-z-map "m" 'changemode-map)
(define-key control-z-map "R" 'open-reference)
;; C-c C-c
(keymap-global-set "C-c C-d" 'crux-duplicate-and-comment-current-line-or-region)
(keymap-global-set "C-c C-l" 'orgit-store-link)
(keymap-global-set "C-c C-w" 'find-file-other-window)
(keymap-global-set "C-c C-o" 'find-file-other-frame)  ; normally bound to (delete-blank-lines)
(keymap-global-set "C-c C-v" 'vterm-execute-region-or-current-line)
(keymap-global-set "C-c C-n" 'doc-view-other-next-page)
;; Control-Shift Tab Movement
(keymap-global-set "C-S-t" 'tab-new)
(keymap-global-set "C-S-w" 'tab-close)
(keymap-global-set "C-S-<right>"'tab-next)
(keymap-global-set "C-S-<left>"'tab-previous)
;;(keymap-global-set "C-S-<right>" 'tab-line-switch-to-next-tab)
;;(keymap-global-set "C-S-<left>" 'tab-line-switch-to-prev-tab)

;; org mode keys
(with-eval-after-load "org"
  (define-key org-mode-map (kbd "C-<tab>") 'org-global-cycle)
  (define-key org-mode-map (kbd "C-c C-s") 'org-insert-src-block) ; default is org-schedule
  (define-key org-mode-map (kbd "C-c C-n") 'doc-view-other-next-page)
  )

;; Functions
(defun org-publish-notes ()
  "Transform org notes to HTML."
  (interactive)
  (org-publish "notes" t))

(defun open-note ()
  "Open file in org directory."
  (interactive)
  (find-file (completing-read "open note: " (directory-files-recursively org-directory "")))
  )

(defun open-reference ()
  "Open file in Sync/reference directory."
  (interactive)
  (find-file (completing-read "open file: " (nconc
                                             (directory-files-recursively "~/code/reference" "")
                                             (directory-files-recursively "~/code/snippets" "")
                                             (directory-files-recursively "~/code/org" "")
                                             ))))

(defun search-notes ()
  "Ripgrep over `org-directory`."
  (interactive)
  (consult-ripgrep org-directory))

(defun search-sync ()
  "Ripgrep over ~/Sync."
  (interactive)
  (consult-ripgrep "~/Sync"))

(defun fd-sync ()
   "`consult-fd` ~/Sync."
  (interactive)
  (let ((default-directory (expand-file-name "~/Sync")))
        (call-interactively 'consult-fd)
        )
  )

(defun open-notes-dir ()
  "Open ~/Sync/org."
  (interactive)
  (pop-to-buffer (find-file-noselect (f-canonical org-directory)))
  )

(defmacro when-on-host (hostname &rest body)
  "Evaluate BODY when /etc/hostname is equal to HOSTNAME."
  `(when (and (f-exists? "/etc/hostname") (equal ,hostname (s-trim (f-read "/etc/hostname")))) ,@body))

(defun buffer-whole-string (buffer-or-name)
  "Return string of BUFFER-OR-NAME."
  (with-current-buffer (get-buffer buffer-or-name)
    (save-restriction
      (widen)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun create-new-scratch-buffer (lang)
  "Create a scratch buffer for the provided LANG."
  (let ((buf (generate-new-buffer (concat "*" lang "*"))))
    (switch-to-buffer buf)
    (funcall (intern(concat lang "-mode")))))

(defun scratch-buffer-menu ()
  "Create new scratch buffer with language menu."
  (interactive)
  (let*
      ((langs my-scratch-buffer-menu-options)
       (choice (completing-read "Language: " langs)))
    (create-new-scratch-buffer choice)))

(defun indent-buffer ()
  "Indent the whole buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(defun control-c ()
  "Copy entire buffer to clipboard."
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max)))

(defun show-buffer-file-name ()
  "Display the buffer file name."
  (interactive)
  (if buffer-file-name
      (progn
	(kill-new buffer-file-name)
	(message "%s" buffer-file-name))
    (message "buffer is not visiting a file")))

(defun change-theme()
  "Change the current theme."
  (interactive)
  (mapc #'disable-theme custom-enabled-themes)
  (call-interactively 'load-theme))

(defun view-url (url)
  "View content of URL in a buffer."
  (interactive "sURL> ")
  (switch-to-buffer (url-retrieve-synchronously url 'inhibit-cookies)))

(defun org-insert-src-block ()
  "Insert #+begin_src."
  (interactive)
  (let ((choice (completing-read "Language: " my-scratch-buffer-menu-options)))
    (insert (concat "#+begin_src " choice "\n\n#+end_src"))
    (previous-line)
    ))

(defun treesit-install-language-grammars ()
  "Install all grammars in `treesit-language-source-alist`."
  (interactive)
  (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist)))

(defun ddg (query)
  "Search DuckDuckGo for QUERY."
  (interactive "sDuckDuckGo for: ")
  (let ((url-format "https://lite.duckduckgo.com/lite/?q=%s"))
    (eww (format url-format (url-hexify-string query)))))

(defun sort-words ()
  "Sort words in a region alphabetically.
Also see `sort-paragraphs`, `sort-lines`, and `sort-subr`."
  (interactive "*")
  (when (use-region-p)
    (sort-regexp-fields nil "\\w+" "\\&" (region-beginning) (region-end))))

(defun sort-symbols ()
  "Sort symbols in region alphabetically."
  (interactive "*")
  (when (use-region-p)
    (sort-regexp-fields nil "\\(\\sw\\|\\s_\\)+" "\\&" (region-beginning) (region-end))))

(defun rails-test-current-file ()
  "Run rails test for file in current buffer."
  (interactive)
  (when (equal "test.rb" (substring (buffer-file-name) (- (length (buffer-file-name)) 7)))
    (async-shell-command (concat "bundle exec rails test " (buffer-file-name)))
    ))

(defun rails-restart-server ()
  "Start or restart rails server for current project."
  (interactive)
  (let* ((process-name (concat (project-name (project-current)) "-rails-server"))
	 (process (get-process process-name))
	 (bundle-path (if (f-exists? (f-join (f-expand (project-root (project-current))) "bin/bundle"))
			  (f-join (f-expand (project-root (project-current))) "bin/bundle")
			"bundle")))
    (if (process-live-p process)
	(progn
	  (message "restarting %s" process-name)
	  (signal-process process 'SIGUSR2))
      (progn
	(message "starting %s" process-name)
	(unless (getenv "SECRET_KEY_BASE")
	  (setenv "SECRET_KEY_BASE" (s-trim (shell-command-to-string "bundle exec rails secret"))))
	(let ((p (start-process process-name (concat "*" process-name "*") bundle-path "exec" "rails" "server")))
	  (with-current-buffer (process-buffer p)
	    (display-buffer (current-buffer))
	    (shell-mode)
	    (read-only-mode)
	    (set-process-filter p 'comint-output-filter))
	  )))))

(defun rails-console ()
  "Launch rails console for current project."
  (interactive)
  (inf-ruby-console-rails (f-expand (project-root (project-current)))))

(defun ansi-colorize-buffer ()
  "Convert ansi escape codes to color text."
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

(defun notepad()
  "Open notepad."
  (interactive)
  (pop-to-buffer (or (get-buffer "*notepad*")
		     (let ((buf (get-buffer-create "*notepad*")))
		       (with-current-buffer buf
			 (text-mode)
			 (jinx-mode)
			 )
		       buf))))

(defun add-underscore-to-beginning-of-filename ()
  "Add underscore to beginning of filename."
  (interactive)
  (let ((f (buffer-file-name)))
    (when (and (file-exists-p f) (not (s-starts-with? "_" (f-base f))))
      (let ((new-filename (f-join (f-dirname f) (concat "_" (f-filename f)))))
	(rename-file f new-filename)
	(set-visited-file-name new-filename t t)
	))))

;; shell
(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'N))

(defun rerun-last-shell-command ()
  "Execute the last shell command from 'command-history' again."
  (interactive)
  (eval (--first (-contains? '(async-shell-command shell-command) (car it)) command-history)))

(defun bash (command)
  "Run bash -c COMMAND."
  (if (equal (call-process "bash" nil nil nil "-c" command) 0)
      t
    (error (concat "command failed: " command))))

(defun fish (command)
  "Run fish -c COMMAND."
  (if (equal (call-process "fish" nil nil nil "-c" command) 0)
      t
    (error (concat "command failed: " command))))

(defun bash-vterm ()
  "Open a bash terminal."
  (interactive)
  (let ((shell-file-name "/usr/bin/bash"))
    (vterm "**bash***")
    ))

(defun fish-vterm ()
  "Open a fish terminal."
  (interactive)
  (let ((shell-file-name "/usr/bin/fish"))
    (vterm "**fish**")
    ))

(defun vterm-remote-shell (host)
  "Open shell on remote HOST."
  (interactive "sHost: ")
  (let ((default-directory (concat "/ssh:" host ":"))
	;;(process-environment (cons "TERM=xterm-256color" process-environment))
        )
    (vterm (concat "*vterm--ssh-" host "*"))
  ))

(defun vterm-doas ()
  "Open a root shell using doas."
  (interactive)
  (let ((default-directory "/doas::"))
    (vterm "*vterm--root*")
    ))

(defun vterm--project-name ()
  (if (project-current)
      (concat "*vterm--" (project-name (project-current)) "*")
    (user-error "no current project"))
  )

(defun vterm-execute-region-or-current-line ()
  "Insert text of current line in vterm and execute."
  (interactive)
  (let ((command (if (region-active-p)
                     (string-trim (buffer-substring
                                   (save-excursion (region-beginning))
                                   (save-excursion (region-end))))
                   (string-trim (buffer-substring (save-excursion
                                                    (beginning-of-line)
                                                    (point))
                                                  (save-excursion
                                                    (end-of-line)
                                                    (point))))))
        (buffer-name (if (project-current) (vterm--project-name) vterm-buffer-name)))
    (let ((buf (current-buffer)))
      (unless (get-buffer buffer-name)
        (vterm buffer-name))
      (display-buffer buffer-name t)
      (switch-to-buffer-other-window buffer-name)
      (vterm--goto-line -1)
      (message command)
      (vterm-send-string command)
      (vterm-send-return)
      (switch-to-buffer-other-window buf)
      )))

(defun project-vterm ()
  "Start an vterm in the current project's root directory."
  (interactive)
  (let ((default-directory (project-root (project-current t)))
         (vterm-buffer (get-buffer (vterm--project-name))))
    (if (and vterm-buffer (not current-prefix-arg))
        (pop-to-buffer vterm-buffer)
      (vterm (generate-new-buffer-name (vterm--project-name)))
      )))

(defun project-vterm-or-vterm ()
  "Use 'project-vterm' when in a project."
  (interactive)
  (if (project-current) (project-vterm) (vterm)))

(defun project-fd ()
  "consult-fd with project."
  (interactive)
  (if (project-current)
      (let ((default-directory (project-root (project-current))))
        (call-interactively 'consult-fd))))

(defun toggle-maximize-buffer ()
  "Maximize buffer."
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))


(defvar number-of-upcoming-days 6 "How far?")

(defun insert-upcoming-days ()
  "Insert string of next X days."
  (interactive)
  (require 'ts)
  (insert (s-join "\n"
		  (-map
		   (lambda (t) (concat (ts-day-name t) " (" (ts-format "%Y-%m-%d" t) ")" ))
		   (-map (lambda (i) (ts-adjust 'day i (ts-now))) (number-sequence 0 number-of-upcoming-days))))))

(defvar my-snippet nil "Text snippet to be inserted by M-<f8>.")

(defun my-snippet-insert ()
  "Insert `my-snippet` into the buffer."
  (interactive)
  (if (s-blank? my-snippet)
      (message "my-snippet is blank. Define it with my-snippet-set.")
    (insert my-snippet)))

(defun my-snippet-set (start end)
  "Set `my-snippet` using current region."
  (interactive "r")
  (setq my-snippet (buffer-substring start end)))

(defun fetch-email ()
  "You've got mail."
  (interactive)
  (async-start
   (lambda ()
     (shell-command-to-string "mbsync -a && notmuch new"))

   (lambda (result)
     (message (-last-item (s-lines (s-trim result)))))))

;; Mailing lists
(defvar my-mailing-lists '() "My mailing lists.")

(defun insert-mailing-list ()
  "Select and insert the addresses from a mailing list."
  (interactive)
  (insert (s-join "," (sort (cdr (assoc-string (completing-read "mailing list:" (-map #'car my-mailing-lists)) my-mailing-lists)) 'string<))))

(defun take-screenshot ()
  "Save a screenshot of the current frame as an SVG image in ~/Screenshots."
  (interactive)
  (let ((filename (f-join "~/Screenshots/" (concat "emacs-" (ts-format "%Y-%m-%dT%H:%M:%S") ".svg"))))
    (with-temp-file filename
      (insert (x-export-frames nil 'svg)))
    (kill-new filename)
    (message filename)))

(defun doc-view-other-next-page ()
  "Go to next page of PDF on opposite window."
  (interactive)
  (with-selected-window (other-window-for-scrolling)
    (if (equal 'doc-view-mode major-mode)
        (doc-view-next-page)
      )))

(defun find-file-in-quotes-other-window ()
  "Find path in \"quotes\" in current line and open it in other window."
  (interactive)
  (let* (
         (line (buffer-substring-no-properties (line-beginning-position) (line-end-position)))
         (match (string-match (rx "\"" (group (0+ (or (1+ (not (any "\"" "\\"))) (seq "\\" anything)))) "\"") line))
         (path (match-string 1 line))
         )
    (find-file-other-window path)
    ))

;; hideshow helpers source https://karthinks.com/software/simple-folding-with-hideshow/
(defun hs-cycle (&optional level)
  "Cycle between folds, optionally set LEVEL."
  (interactive "p")
  (let (message-log-max
        (inhibit-message t))
    (if (= level 1)
        (pcase last-command
          ('hs-cycle
           (hs-hide-level 1)
           (setq this-command 'hs-cycle-children))
          ('hs-cycle-children
           ;; TODO: Fix this case. `hs-show-block' needs to be
           ;; called twice to open all folds of the parent
           ;; block.
           (save-excursion (hs-show-block))
           (hs-show-block)
           (setq this-command 'hs-cycle-subtree))
          ('hs-cycle-subtree
           (hs-hide-block))
          (_
           (if (not (hs-already-hidden-p))
               (hs-hide-block)
             (hs-hide-level 1)
             (setq this-command 'hs-cycle-children))))
      (hs-hide-level level)
      (setq this-command 'hs-hide-level))))

(defun hs-global-cycle ()
  "Global cycle hide-show."
    (interactive)
    (pcase last-command
      ('hs-global-cycle
       (save-excursion (hs-show-all))
       (setq this-command 'hs-global-show))
      (_ (hs-hide-all))))

;; Startup hook

(defun my-startup-hook ()
  "On startup."
  (when (not (display-graphic-p))
    (global-hl-line-mode -1)
    (corfu-terminal-mode +1)
    )

  (when-on-host "box"
                (when (f-exists? personal-elisp-file)
                  (load-file personal-elisp-file))

                (when (daemonp)
                  (load-theme 'ef-dream t))

                (when (and (display-graphic-p) (not (daemonp)))
                  ;; (desktop-save-mode t)
                  ;; (desktop-read)
                  (mapc #'disable-theme custom-enabled-themes)
                  (load-theme 'ef-deuteranopia-dark t))
                )

  (when-on-host "laptop"
		(load-theme 'ef-bio t)
		)

  (print (concat "Startup Complete. Init time: " (emacs-init-time)) #'external-debugging-output)
  )

(add-hook 'emacs-startup-hook #'my-startup-hook)

;;; init.el ends here
