if ENV['INSIDE_EMACS']
  IRB.conf[:USE_MULTILINE] = false
  IRB.conf[:USE_READLINE] = false
end
