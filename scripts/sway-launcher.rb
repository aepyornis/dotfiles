#!/usr/bin/env ruby

require 'open3'

FONT = "/usr/share/fonts/truetype/dejavu/DejaVuSerif.ttf"
LAUNCHER = "tofi --font #{FONT}"
APPS_PATH = File.expand_path("~/.config/sway/apps")

apps = File.readlines(APPS_PATH)
         .delete_if { |x| x.strip == '' || x[0] == '#' }
         .map(&:strip)
         .map { |x| x.split(' ', 2) }

choice = Open3.popen2(LAUNCHER) do |stdin, stdout|
  stdin.puts apps.map(&:first).join("\n")
  stdin.close
  stdout.gets&.chomp
end

apps.each do |(name, cmd)|
  if name == choice
    puts cmd
    exit
  end
end

exit 1
