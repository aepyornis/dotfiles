#!/bin/sh
# show/hide <app_id> from clipboard
#   $ sway-toggle-app.sh signal

app_id="$1"
visible_workspace=$(swaymsg -t get_workspaces | jq '.[] | select(.visible) | .id')

if swaymsg -t get_tree | jq -e ".. | (.nodes? // empty )[] | select(.type == \"workspace\" and .id == $visible_workspace) | [.nodes,.floating_nodes] | flatten | .[] | select(.app_id == \"$app_id\") | any" > /dev/null
then
    swaymsg [app_id="\"$app_id\""] move to scratchpad
else
    swaymsg [app_id="\"$app_id\""] move to workspace current
    swaymsg focus floating
fi
