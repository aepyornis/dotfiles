#!/bin/sh

swaymsg -t get_tree | jq -r 'recurse(.nodes[]?) | recurse(.floating_nodes[]?) | .app_id | select(.)'
