#!/usr/bin/env fish
set -x dotfiles (path resolve ~/dotfiles)

if not test -d $dotfiles
    echo "missing $dotfiles" >&2
    exit 1
end

echo "dotfiles: $dotfiles" >&2
cd ~

mkdir -p ~/.config ~/.local ~/.local/opt ~/.local/bin ~/.local/src ~/.local/lib ~/.local/www ~/.local/share ~/.local/share/themes ~/.local/share/fonts
mkdir -p ~/.terminfo ~/.cache/fetch ~/.cache/mpd ~/.cache/msmtp ~/.cache/1000

# install scripts and services
find $dotfiles/scripts -type f | xargs -I Script ln -v -f -s Script ~/.local/bin
find $dotfiles/services -type f | xargs -I File ln -v -f -s File ~/.config/systemd/user
# find $dotfiles/desktop -type f -name '*.desktop' | xargs -I File desktop-file-install --dir ~/.local/share/applications/ File

# link config files
# $dotfiles/config/.irbc ->  ~/.irbc
find $dotfiles/config -maxdepth 1 -type f | xargs -I File fish -c "ln -v -f -s File $HOME/(path basename File)"
# $dotfiles/config/git/ignore -> ~/.config/git/ignore
find $dotfiles/config -mindepth 1 -maxdepth 1 -type d | xargs -I Dir fish -c "mkdir -p $HOME/.config/(path basename Dir)"
find $dotfiles/config -mindepth 2 -maxdepth 2 -type f | xargs -I File fish -c 'ln -v -f -s File ~/.config/(string split "/" File | tail -n 2 | string join "/")'
find $dotfiles/config -mindepth 2 -maxdepth 2 -type d | xargs -I Dir fish -c 'ln -v -f -T -s Dir ~/.config/(string split "/" Dir | tail -n 2 | string join "/")'
