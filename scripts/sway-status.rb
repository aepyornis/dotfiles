#!/usr/bin/env -S ruby --yjit
$stdout.sync = true

BATTERY_PATH = '/sys/class/power_supply/BAT0'
HAS_BATTERY = File.exist?(BATTERY_PATH)
HAS_NOTMUCH = system("which notmuch > /dev/null")

loop do
  status_bar = []

  if HAS_NOTMUCH
    if `notmuch count tag:unread`.to_i > 0
      status_bar << "📧"
    end
  end

  if HAS_BATTERY
    if File.read(File.join(BATTERY_PATH, 'status')) == 'Discharging'
      status_bar << (File.read(File.join(BATTERY_PATH, 'capacity')).chomp + '🔋')
    else
      status_bar << "⌁"
    end
  end

  load1, load5, load15 = File.read('/proc/loadavg').split(' ').slice(0,3).map { |l| l.to_f.round }

  status_bar << "#{load1},#{load15}"

  status_bar << Time.now.strftime('%H:%M %Y-%m-%d')

  puts status_bar.join(' · ')
  sleep 10
end
