#!/usr/bin/env ruby

require 'open3'

password_dir = File.join(Dir.home, ".password-store")

launcher_cmd = "tofi --prompt-text \"password: \" --font /usr/share/fonts/truetype/noto/NotoSans-Italic.ttf"

selection = Open3.popen2(launcher_cmd) do |stdin, stdout|
  Dir[File.join(password_dir, "/**/*.gpg")].each do |file|
    stdin.puts file[password_dir.length+1..-5]
  end
  stdin.close
  stdout.gets&.chomp
end

exec "pass -c #{selection}" if selection
